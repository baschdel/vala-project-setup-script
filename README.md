# Vala Project Setup Script

This script automates setting up vala projects using the meson build-system.

## Features
- Automatic generation of meson.build files
- One command to get started
- One command to update buildfiles
- One command to build and run
- Generates an initial README.md wich explains how to use the build-system
- Optionally generates a hello world program
- Sets up a git repository and makes the initial commit

## How to use
Execute the `setup_project.lua` file from the folder the project is supposed to be in and answer all the questions.

All scripts in this package MUST be run from the projects root folder!

It will ask you for:

### The projects name
This is the name the final executable should have, use lowercase letters

### The reverse domain of the projects home
Use the reverse domain notation of your internet presence.
If your website is at `example.org` use `org.example`
if you only have an account for example on `git*.com` use `com.git*.your_username`

Your projects "id" will then be `<reverse_domain>.<project_name>`

### Create a main.vala file

If you respod with y the script will put a main.vala containing a simple hello world program in the src folder, similar to what cargo does for rust

If you enabled this one and after the setup is finished run the `run` script the last line should be `Hello World from <project_name>!`.

### Add rename.sh

If enabled it will put a powerful renaming script in the projects root folder that allows you to easily run a sed command across all files whose name ends in `.vala`. Make sure to handle it with care, it will run your day otherwise (You have been warned)!

Usage:
`./rename.sh <text_to_replace> <replace_with>`

This is inteded to be used to rename classes and interfaces across the whole project, but since it just uses sed under the hood you can use it to replace anything with eyerything. Be careful to not forget the second argument because the everything includes nothing too.

### Initialize git repository

If this one is enabled the script will automatically run `git init` after it created all other files, install a gitignore file and make the first commit.

The next question will be if you want to add a remote repository, if you give a positive response it'll ask you for the remote address. This is the same address you'd use to git clone the repository.
Make sure the remote repository is completely empty, otherwise you'll run into problems later on.

After the script has run you have to make the first push yourself. The command `git push -u origin master` should do the trick here, if not your git service should provide you with instructions on how to do it.
(Note: the script already ran the command `git remote add origin <address>`)

### Is this information correct?
After answering all questions the script will present you with a summary of all relevant information it is going to use along with a list of enabled fetaures. Answering anything other than "y" here will terminate the script. If you answer with y it will continue adding all files and executing commands.

Check the output for any errors, currently the script assumes everything went well with the previous command!

## Do I have to keep this script?

No, after you have initalized a peoject, you don't have to keep the script, nothing it generates will depend on the setup script bein present. (That's the reason its name is in the gitignore file)

## How to use the resulting build tools

There are three build tools that will be put in place:

### update_src_build_files
if you look at the meson.build file in the source directory you'll notice some `#AUTOUPDATE_FILE` comments. This script is the one that takes care of these. When run it will remove those entries and replace them with an up to date list of all files in the `src/` folder ending in `.vala`.
If for some reason this script messes up, you can find a backup file from before it was last run in the src( folder as meson.build.old

### build
This one will setup a meson_build folder, run ninja inside it and put the resulting binary in the projects root folder, if for some reason it does not work after pulling a big update try deleting the meson_build folder.

### run
This script automatically calls the build script and executes the resulting binary.
It helps when doing a lot of testing.

If there already is a binary in the projects root folder it will be moved to `<project_name>.old`
